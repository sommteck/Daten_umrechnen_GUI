namespace Daten_umrechnen_GUI
{
    partial class Daten_umrechnen
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_obergrenze = new System.Windows.Forms.TextBox();
            this.txt_untergrenze = new System.Windows.Forms.TextBox();
            this.cmd_show = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.SP_Byte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_Kilobyte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_Megabyte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_Gigabyte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_Terabyte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_schrittweite = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "MB Untergrenze:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "MB Obergrenze:";
            // 
            // txt_obergrenze
            // 
            this.txt_obergrenze.Location = new System.Drawing.Point(154, 80);
            this.txt_obergrenze.Name = "txt_obergrenze";
            this.txt_obergrenze.Size = new System.Drawing.Size(100, 22);
            this.txt_obergrenze.TabIndex = 2;
            // 
            // txt_untergrenze
            // 
            this.txt_untergrenze.Location = new System.Drawing.Point(154, 42);
            this.txt_untergrenze.Name = "txt_untergrenze";
            this.txt_untergrenze.Size = new System.Drawing.Size(100, 22);
            this.txt_untergrenze.TabIndex = 1;
            // 
            // cmd_show
            // 
            this.cmd_show.Location = new System.Drawing.Point(34, 232);
            this.cmd_show.Name = "cmd_show";
            this.cmd_show.Size = new System.Drawing.Size(87, 30);
            this.cmd_show.TabIndex = 4;
            this.cmd_show.Text = "&Anzeigen";
            this.cmd_show.UseVisualStyleBackColor = true;
            this.cmd_show.Click += new System.EventHandler(this.cmd_show_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(34, 289);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(87, 32);
            this.cmd_clear.TabIndex = 5;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(34, 346);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(87, 28);
            this.cmd_end.TabIndex = 6;
            this.cmd_end.Text = "&Beenden";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SP_Byte,
            this.SP_Kilobyte,
            this.SP_Megabyte,
            this.SP_Gigabyte,
            this.SP_Terabyte});
            this.dataGridView1.Location = new System.Drawing.Point(287, 42);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(543, 344);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.TabStop = false;
            // 
            // SP_Byte
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.SP_Byte.DefaultCellStyle = dataGridViewCellStyle6;
            this.SP_Byte.HeaderText = "Byte";
            this.SP_Byte.Name = "SP_Byte";
            this.SP_Byte.ReadOnly = true;
            // 
            // SP_Kilobyte
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.SP_Kilobyte.DefaultCellStyle = dataGridViewCellStyle7;
            this.SP_Kilobyte.HeaderText = "Kilobyte";
            this.SP_Kilobyte.Name = "SP_Kilobyte";
            this.SP_Kilobyte.ReadOnly = true;
            // 
            // SP_Megabyte
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.SP_Megabyte.DefaultCellStyle = dataGridViewCellStyle8;
            this.SP_Megabyte.HeaderText = "Megabyte";
            this.SP_Megabyte.Name = "SP_Megabyte";
            this.SP_Megabyte.ReadOnly = true;
            // 
            // SP_Gigabyte
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.SP_Gigabyte.DefaultCellStyle = dataGridViewCellStyle9;
            this.SP_Gigabyte.HeaderText = "Gigybyte";
            this.SP_Gigabyte.Name = "SP_Gigabyte";
            this.SP_Gigabyte.ReadOnly = true;
            // 
            // SP_Terabyte
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.SP_Terabyte.DefaultCellStyle = dataGridViewCellStyle10;
            this.SP_Terabyte.HeaderText = "Terabyte";
            this.SP_Terabyte.Name = "SP_Terabyte";
            this.SP_Terabyte.ReadOnly = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Schrittweite:";
            // 
            // txt_schrittweite
            // 
            this.txt_schrittweite.Location = new System.Drawing.Point(154, 126);
            this.txt_schrittweite.Name = "txt_schrittweite";
            this.txt_schrittweite.Size = new System.Drawing.Size(100, 22);
            this.txt_schrittweite.TabIndex = 3;
            this.txt_schrittweite.Text = "1";
            // 
            // Daten_umrechnen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 431);
            this.Controls.Add(this.txt_schrittweite);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_show);
            this.Controls.Add(this.txt_untergrenze);
            this.Controls.Add(this.txt_obergrenze);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Daten_umrechnen";
            this.Text = "Daten umrechnen";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_obergrenze;
        private System.Windows.Forms.TextBox txt_untergrenze;
        private System.Windows.Forms.Button cmd_show;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_Byte;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_Kilobyte;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_Megabyte;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_Gigabyte;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_Terabyte;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_schrittweite;
    }
}

