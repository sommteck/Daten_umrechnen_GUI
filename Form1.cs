using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Daten_umrechnen_GUI
{
    public partial class Daten_umrechnen : Form
    {
        public Daten_umrechnen()
        {
            InitializeComponent();

            ToolTip help = new ToolTip();
            help.SetToolTip(cmd_show, "Umrechnen");
            help.SetToolTip(cmd_clear, "Ein- und Ausgaben löschen");
            help.SetToolTip(cmd_end, "Programm beenden");
            help.SetToolTip(txt_untergrenze, "Minimalwert eingeben");
            help.SetToolTip(txt_obergrenze, "Maximalwert eingeben");
            help.SetToolTip(dataGridView1, "Ausgabe");
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            txt_untergrenze.Text = txt_obergrenze.Text = null;
            dataGridView1.Rows.Clear();
            txt_untergrenze.Focus();
        }

        private void cmd_show_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            double untergrenze, obergrenze, schrittweite, B, KB, MB, GB, TB;

            try
            {
                untergrenze = Convert.ToDouble(txt_untergrenze.Text);
                obergrenze = Convert.ToDouble(txt_obergrenze.Text);
                schrittweite = Convert.ToDouble(txt_schrittweite.Text);

                for (; untergrenze <= obergrenze; untergrenze = untergrenze + schrittweite)
                {
                    B = untergrenze * 1024 * 1024;
                    KB = untergrenze * 1024;
                    MB = untergrenze;
                    GB = untergrenze / 1024;
                    TB = GB / 1024;

                    dataGridView1.Rows.Add(B.ToString(), KB.ToString("F3"), MB.ToString("F3"), GB.ToString("F6"),
                    TB.ToString("F9"));
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                txt_untergrenze.Text = txt_obergrenze.Text = null;
                txt_untergrenze.Focus();
            }

        }
    }
}